# Test Data

Test data for validating marshallers

- [protocol.1.json](protocol.1.json) - envelopes and accounts, using Merkle hashes for signatures
- [protocol.2.json](protocol.2.json) - envelopes and accounts, using simple hashes for signatures
- [protocol.3.json](protocol.3.json) - same as (2), but without 'virtual' properties that exist for API backwards compatibility
